---
layout: article
title: SpringBoot从0到跑起项目
tags: SpringBoot
---

# Spring Boot 

---

## 核心功能

- 能够独立运行
  - Spring Boot 可以以 jar 包的形式独立运行，运行一个 Spring Boot 项目只需通过 java–jar xx.jar 来运行。
- 内嵌servlet容器
  - Spring Boot 可选择内嵌 Tomcat、Jetty 或者 Undertow，这样我们无须以 war 包形式部署项目。
- 提供starte简化Maven配置
  - Spring 提供了一系列的 starter pom 来简化 Maven 的依赖加载，例如，当你使用了spring-boot-starter-web 时，会自动加入依赖包。
- 自动配置spring
  - Spring Boot 会根据在类路径中的 jar 包、类，为 jar 包里的类自动配置 Bean，这样会极大地减少我们要使用的配置。当然，Spring Boot 只是考虑了大多数的开发场景，并不是所有的场景，若在实际开发中我们需要自动配置 Bean，而 Spring Boot 没有提供支持，则可以自定义自动配置。
- 准生产的应用监控
  - Spring Boot 提供基于 http、ssh、telnet 对运行时的项目进行监控。
- 无码生成和xml配置
  - Spring Boot 的神奇的不是借助于代码生成来实现的，而是通过条件注解来实现的，这是 Spring 4.x 提供的新特性。Spring 4.x 提倡使用 Java 配置和注解配置组合，而 Spring Boot 不需要任何 xml 配置即可实现 Spring 的所有配置。

## 优点与缺点

1. **优点**
   1. 快速构建项目。
   2. 对主流开发框架的无配置集成。
   3. 项目可独立运行，无须外部依赖Servlet容器。
   4. 提供运行时的应用监控。
   5. 极大地提高了开发、部署效率。
   6. 与[云计算](http://c.biancheng.net/cloud_computing/)的天然集成。

1. **缺点**
   1. 版本迭代速度很快，一些模块改动很大。
   2. 由于不用自己做配置，报错时很难定位。
   3. 网上现成的解决方案比较少。

---

## SpringBoot的快速搭建

https://start.spring.io/ 该网站能帮助快读搭建spring项目框架

**也可手动搭建springboot环境**

环境 jdk8+ maven3+